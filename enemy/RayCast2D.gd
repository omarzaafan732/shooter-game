extends RayCast2D

export var mx = 0.70
export var mn = -0.70

var direction = 1

func _ready():
	pass # Replace with function body.


func _process(delta):
	rotate(0.01 * direction)
	rotation = clamp(rotation, mn, mx)
	if (rotation >= mx or rotation <= mn):
		direction *= -1
