extends KinematicBody2D

export var speed : int = 100
export var acc : int = 300
export var frec : int = 400

var velocity = Vector2.ZERO
var bullet = preload("res://bullet/bullet.tscn")

onready var rayCast = $RayCast2D

func _ready():
	pass # Replace with function body.

func _process(delta):
	
	get_parent().offset += speed * delta
	
	if(rayCast.get_collider() == get_parent().get_parent().get_parent().get_node("player")):
		speed = 0
