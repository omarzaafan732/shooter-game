extends KinematicBody2D

var speed : int = 155000
var velocity : Vector2 = Vector2.ZERO
var dirction : Vector2 = Vector2.ZERO

func _ready():
	look_at(get_global_mouse_position())
	dirction = (get_global_mouse_position() - global_position).normalized()

func _process(delta):
	velocity = speed * delta * dirction
	velocity = move_and_slide(velocity)


func _on_Area2D_body_entered(body):
	if (body.is_in_group("enemy")):
		queue_free()
