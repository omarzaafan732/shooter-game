extends KinematicBody2D

export var speed : int = 100
export var acc : int = 300
export var frec : int = 400

var inputVec = Vector2.ZERO
var velocity = Vector2.ZERO
var bullet = preload("res://bullet/bullet.tscn")

func _ready():
	pass # Replace with function body.

func _process(delta):
	inputVec.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	inputVec.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	inputVec.normalized()
	look_at(get_global_mouse_position())
	
	if (inputVec != Vector2.ZERO):
		velocity = velocity.move_toward(speed * inputVec, acc * delta)
	else:
		velocity = velocity.move_toward(Vector2.ZERO, frec * delta)
		
	if(Input.is_action_just_pressed("shoot")):
		var instance = bullet.instance()
		instance.global_position = $Position2D.global_position
		get_parent().add_child(instance)
	
	velocity = move_and_slide(velocity)
	
